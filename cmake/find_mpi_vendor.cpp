#include <mpi.h>
#include <iostream>

int main(){
    char version[MPI_MAX_LIBRARY_VERSION_STRING];
    int version_len;

    int result = MPI_Get_library_version(version, &version_len);
    std::cout << version;
    return 0;
}

#[==============================================================================================[
#                                  Libxc compatibility wrapper                                  #
]==============================================================================================]

# Upstream issues:
# https://gitlab.com/libxc/libxc/-/merge_requests/610
# https://gitlab.com/libxc/libxc/-/merge_requests/604

#[===[.md
# FindLibxc

Libxc compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES Libxc libxc
        PKG_MODULE_NAMES libxcf03)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(Libxc::xcf03 ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET Libxc::xcf03)
    add_library(Libxc::xcf03 ALIAS xcf03)
endif ()

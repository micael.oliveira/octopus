# Building with CMake

This document goes over a few ways to build Octopus using cmake, how to configure it and how to run basic tests

## Table of Contents

1. [Running CMake](#running-cmake)
2. [Configuring the Cmake Build](#configuring-the-cmake-build)
3. [CMake Options](#cmake-options)
4. [Testing](#testing)
5. [Adding Modules to CMakeLists.txt](#adding-modules-to-cmakelists.txt)


## Running cmake

The configure, build, test and install steps are all run through the `cmake` CLI:
```console
$ cmake -B ./build
$ cmake --build ./build
$ ctest --test-dir ./build -L short-run
$ cmake --install ./build
```

This will configure, build and run the tests in the `./build` directory, using the default options, i.e. compiling
without parallel or external library support (more on that in the next sections). If you are using `CMake >= 3.23`
you can use [`cmake-presets`](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html)to simplify this process
and use some pre-defined options, e.g.:
```console
$ cmake --preset default
$ cmake --build --preset default
$ ctest --preset default
$ cmake --install ./cmake-build-release
```

And if you are using `CMake >= 3.25` you can simplify this to a single step (except for `install` which has no presets):
```onsole
$ cmake --workflow --preset default
$ cmake --install ./cmake-build-release
```

You can find a list of these presets using `cmake --list-presets`. Note that some presets are customized for Octopus
buildbot.
```console
$ cmake --list-presets
Available configure presets:

  "default"                   - Default configuration preset
```

## Configuring the cmake build

It is possible to run a GUI like `ccmake` to find all of the options available to configure, but it is encouraged to
see the available options directly in [`/CMakeLists.txt`](../CMakeLists.txt), e.g.:
```cmake
#[==============================================================================================[
#                                            Options                                            #
]==============================================================================================]

option(OCTOPUS_MPI "Octopus: Build with MPI support" OFF)
option(OCTOPUS_OpenMP "Octopus: Build with OpenMP support" OFF)
option(OCTOPUS_INSTALL "Octopus: Install project" ${PROJECT_IS_TOP_LEVEL})
option(OCTOPUS_TESTS "Octopus: Build with unit-tests" ${PROJECT_IS_TOP_LEVEL})
```

To configure these options you prepend a `-D` to the name of the option and pass them at the configure step along with
the value you wish, e.g.:
```console
$ cmake -B ./build -DOCTOPUS_MPI=ON
```

It might be cumbersome to define all of these variables. That is why we provide presets for some common scenarios, e.g.
`default` preset sets `CMAKE_BUILD_TYPE=Release` and the build directory to `cmake-build-release` so the following
command are equivalent:
```console
$ cmake -B ./cmake-build-release -DCMAKE_BUILD_TYPE=Release
$ cmake --preset default
Preset CMake variables:

  CMAKE_BUILD_TYPE:STRING="Release"
```
To find out what variables are set by these presets, simply run the preset, and these variables will be printed at the
beginning (as shown in the snippet above), or navigate through their definition in
[`CMakePresets-defaults.json`](CMakePresets-defaults.json).

And of course you can mix these approached such as:
```console
$ cmake --preset default -DOCTOPUS_MPI=ON
```
But note that this does not work with workflow presets.

### CMake options

The Octopus defined options and what they do are defined in the source at [`/CMakeLists.txt`](../CMakeLists.txt). But
besides these options, there are a few CMake native options that you should consider.

| Option                                    |   Default    | Description                                                                                                                                         |
|:------------------------------------------|:------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------|
| `CMAKE_INSTALL_PREFIX`                    | `/usr/local` | Path where to install Octopus                                                                                                                       |
| `CMAKE_Fortran_Flags`                     |  \<empty\>   | Additional Fortran compiler flags. There are also `C` and `CXX` equivalents.                                                                        |
| `FETCHCONTENT_TRY_FIND_PACKAGE_MODE`      |   `OPT_IN`   | Set to `NEVER` if you want to download all external dependencies, otherwise Octopus will use the system installed ones.                             |
| `FETCHCONTENT_SOURCE_DIR_<uppercaseName>` |  \<empty\>   | Path to the external dependency source to use. E.g. you can set it to the provided submodules:`FETCHCONTENT_SOURCE_DIR_SPGLIB=./third_party/Spglib` |

If you are using downloaded external dependencies, you can configure those packages by simply adding those options e.g.:
`SPGLIB_USE_SANITIZER=address` (Note these options are passed unaltered to the dependencies so be aware of
name-clashes). See the upstream packages for available options.

You can of course define a custom preset that contain all of these options in `/CMakeUserPresets.json`, e.g. a `debug`
preset might look like:
```json
{
  "version": 6,
  "configurePresets": [
    {
      "name": "debug",
      "displayName": "Debug configure",
      "inherits": [
        "default"
      ],
      "binaryDir": "build",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Debug"
      }
    }
  ]
}
```

## Testing

The default `ctest` will run all available tests, but you can select the tests you want using regex or labels:
```console
$ ctest --print-labels
Test project /octopus/cmake-build-release
All Labels:
  components
  errors
  long-run
  short-run
  tutorials
$ ctest -N -L short-run
Test project /octopus/cmake-build-release
  Test   #1: components/01-derivatives_1d
  Test   #2: components/02-derivatives_2d
  Test   #3: components/03-derivatives_3d
$ ctest -N -R '.*octopus_basics-getting_started'
Test project /octopus/cmake-build-release
  Test #259: tutorials/01-octopus_basics-getting_started

Total Tests: 1
```

Note that in this example `-N` was used to show the tests only. To run them, remove that option


## Adding Modules to CMakeLists.txt

A `CMakeLists.txt` file is defined in each subfolder of the `src/`. In order to add a new fortran source file to 
the build, simply append the `target_sources` arguments with the file name:

```Cmake
target_sources(Octopus_lib PRIVATE
               module.F90
               added_module.F90
```

This specifies sources to use when building a target `Octopus_lib`.

# Script to be called by the target in `oct_add_checksum_copy` in `helpers.cmake`
# Check the checksums of all the files in `checksum_src` and copy the files if they differ from `checksum_dest`

# Variables::
#   checksum_src (path): location of the source to copy from
#   checksum_dest (path): location of the source to copy to

# Check if directory does not exist
if (NOT EXISTS ${checksum_dest})
	# If doesn't exist can just copy everything
	message(VERBOSE
			"ChecksumCopy: Destination folder not found. Copying everything\n"
			"${checksum_src} -> ${checksum_dest}")
	file(COPY ${checksum_src} ${checksum_dest})
	return()
endif ()

# Check each file
file(GLOB_RECURSE src_files RELATIVE ${checksum_src} ${checksum_src}/*)
foreach (file IN LISTS src_files)
	file(COPY_FILE ${checksum_src}/${file} ${checksum_dest}/${file}
			ONLY_IF_DIFFERENT)
endforeach ()

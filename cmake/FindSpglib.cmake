#[==============================================================================================[
#                                 Spglib compatibility wrapper                                 #
]==============================================================================================]

#[===[.md
# FindSpglib

Spglib compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES Spglib spglib
        PKG_MODULE_NAMES spglib_f08)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(Spglib::fortran ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET Spglib::fortran)
    add_library(Spglib::fortran ALIAS spglib_f08)
endif ()

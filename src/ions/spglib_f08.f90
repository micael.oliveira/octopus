module spglib_f08

  use iso_c_binding, only:  c_char, c_int, c_double, c_ptr, c_null_char, c_f_pointer, c_associated

  implicit none

  private

  enum, bind(C)
    enumerator ::  SPGLIB_SUCCESS = 0
    enumerator ::  SPGERR_SPACEGROUP_SEARCH_FAILED
    enumerator ::  SPGERR_CELL_STANDARDIZATION_FAILED
    enumerator ::  SPGERR_SYMMETRY_OPERATION_SEARCH_FAILED
    enumerator ::  SPGERR_ATOMS_TOO_CLOSE
    enumerator ::  SPGERR_POINTGROUP_NOT_FOUND
    enumerator ::  SPGERR_NIGGLI_FAILED
    enumerator ::  SPGERR_DELAUNAY_FAILED
    enumerator ::  SPGERR_ARRAY_SIZE_SHORTAGE
    enumerator ::  SPGERR_NONE
  end enum

  type :: SpglibSpacegroupType
    integer(c_int) :: number
    character(len=11) :: international_short
    character(len=20) :: international_full
    character(len=32) :: international
    character(len=7) :: schoenflies
    character(len=17) :: hall_symbol
    character(len=6) :: choice
    character(len=6) :: pointgroup_international
    character(len=4) :: pointgroup_schoenflies
    integer(c_int) :: arithmetic_crystal_class_number
    character(len=7) :: arithmetic_crystal_class_symbol
  end type SpglibSpacegroupType

  type :: SpglibDataset
    integer(c_int) :: spacegroup_number
    integer(c_int) :: hall_number
    character(len=11) :: international_symbol
    character(len=17) :: hall_symbol
    character(len=6) :: choice
    real(c_double)  :: transformation_matrix(3, 3)
    real(c_double)  :: origin_shift(3)
    integer(c_int) :: n_operations
    integer(c_int), allocatable :: rotations(:, :, :)
    real(c_double), allocatable :: translations(:, :)
    integer(c_int) :: n_atoms
    integer(c_int), allocatable :: wyckoffs(:)
    integer(c_int), allocatable :: equivalent_atoms(:) !Beware mapping refers to positions starting at 0
    integer(c_int) :: n_std_atoms
    real(c_double) :: std_lattice(3, 3)
    integer(c_int), allocatable :: std_types(:)
    real(c_double), allocatable :: std_positions(:, :)
    character(len=6) :: pointgroup_symbol
    integer(kind(SPGLIB_SUCCESS)) :: spglib_error
  end type SpglibDataset

  interface

    function spg_get_error_code() bind(c) result(error_code)
      import SPGLIB_SUCCESS
      integer(kind(SPGLIB_SUCCESS)) :: error_code
    end function spg_get_error_code

  end interface



  public :: SpglibDataset, spg_get_dataset, &
  & SpglibSpacegroupType, spg_get_spacegroup_type, &
  & spg_get_error_code, spg_get_error_message


contains

  function spg_get_error_message(spglib_error) result(error_message)
    integer(kind(SPGLIB_SUCCESS)) :: spglib_error
    character(len=32) :: error_message

    character, pointer, dimension(:) :: message
    type(c_ptr) :: message_ptr

    integer :: i

    interface

      function spg_get_error_message_c(spglib_error_c) &
      & bind(c, name='spg_get_error_message') result(error_message_c)
        import c_ptr, SPGLIB_SUCCESS

        integer(kind(SPGLIB_SUCCESS)), value :: spglib_error_c
        type(c_ptr) :: error_message_c

      end function spg_get_error_message_c

    end interface

    message_ptr = spg_get_error_message_c(spglib_error)
    call c_f_pointer(message_ptr, message, [len(error_message)])

    error_message = ' '
    do i = 1, len(error_message)
      if (message(i) == C_NULL_CHAR) exit
      error_message(i:i) = message(i)
    end do

  end function spg_get_error_message

  function spg_get_spacegroup_type(hall_number) result(spgtype)
    integer(c_int), intent(in) :: hall_number
    type(SpglibSpacegroupType) :: spgtype

    type, bind(c) :: SpglibSpacegroupType_c
      integer(c_int) :: number
      character(kind=c_char) :: international_short(11)
      character(kind=c_char) :: international_full(20)
      character(kind=c_char) :: international(32)
      character(kind=c_char) :: schoenflies(7)
      character(kind=c_char) :: hall_symbol(17)
      character(kind=c_char) :: choice(6)
      character(kind=c_char) :: pointgroup_international(6)
      character(kind=c_char) :: pointgroup_schoenflies(4)
      integer(c_int) :: arithmetic_crystal_class_number
      character(kind=c_char) :: arithmetic_crystal_class_symbol(7)
    end type SpglibSpacegroupType_c

    interface

      function spg_get_spacegroup_type_c(hall_number_c) &
      & bind(c, name='spg_get_spacegroup_type') result(spgtype_c)
        import c_int, SpglibSpacegroupType_c
        integer(c_int), intent(in), value :: hall_number_c
        type(SpglibSpacegroupType_c) :: spgtype_c
      end function spg_get_spacegroup_type_c

    end interface

    type(SpglibSpacegroupType_c), allocatable:: spgtype_c
    integer :: i

    allocate (spgtype_c, source=spg_get_spacegroup_type_c(hall_number))

    spgtype%number = spgtype_c%number
    spgtype%arithmetic_crystal_class_number = &
    & spgtype_c%arithmetic_crystal_class_number

    do i = 1, size(spgtype_c%international_short)
      if (spgtype_c%international_short(i) == C_NULL_CHAR) then
        spgtype%international_short(i:) = ' '
        exit
      end if
      spgtype%international_short(i:i) = spgtype_c%international_short(i)
    end do

    do i = 1, size(spgtype_c%international_full)
      if (spgtype_c%international_full(i) == C_NULL_CHAR) then
        spgtype%international_full(i:) = ' '
        exit
      end if
      spgtype%international_full(i:i) = spgtype_c%international_full(i)
    end do

    do i = 1, size(spgtype_c%international)
      if (spgtype_c%international(i) == C_NULL_CHAR) then
        spgtype%international(i:) = ' '
        exit
      end if
      spgtype%international(i:i) = spgtype_c%international(i)
    end do

    do i = 1, size(spgtype_c%schoenflies)
      if (spgtype_c%schoenflies(i) == C_NULL_CHAR) then
        spgtype%schoenflies(i:) = ' '
        exit
      end if
      spgtype%schoenflies(i:i) = spgtype_c%schoenflies(i)
    end do

    do i = 1, size(spgtype_c%hall_symbol)
      if (spgtype_c%hall_symbol(i) == C_NULL_CHAR) then
        spgtype%hall_symbol(i:) = ' '
        exit
      end if
      spgtype%hall_symbol(i:i) = spgtype_c%hall_symbol(i)
    end do

    do i = 1, size(spgtype_c%choice)
      if (spgtype_c%choice(i) == C_NULL_CHAR) then
        spgtype%choice(i:) = ' '
        exit
      end if
      spgtype%choice(i:i) = spgtype_c%choice(i)
    end do

    do i = 1, size(spgtype_c%pointgroup_international)
      if (spgtype_c%pointgroup_international(i) == C_NULL_CHAR) then
        spgtype%pointgroup_international(i:) = ' '
        exit
      end if
      spgtype%pointgroup_international(i:i) = &
      & spgtype_c%pointgroup_international(i)
    end do

    do i = 1, size(spgtype_c%pointgroup_schoenflies)
      if (spgtype_c%pointgroup_schoenflies(i) == C_NULL_CHAR) then
        spgtype%pointgroup_schoenflies(i:) = ' '
        exit
      end if
      spgtype%pointgroup_schoenflies(i:i) = &
      & spgtype_c%pointgroup_schoenflies(i)
    end do

    do i = 1, size(spgtype_c%arithmetic_crystal_class_symbol)
      if (spgtype_c%arithmetic_crystal_class_symbol(i) == C_NULL_CHAR) then
        spgtype%arithmetic_crystal_class_symbol(i:) = ' '
        exit
      end if
      spgtype%arithmetic_crystal_class_symbol(i:i) = &
      & spgtype_c%arithmetic_crystal_class_symbol(i)
    end do

  end function spg_get_spacegroup_type

  function spg_get_dataset(lattice, position, types, num_atom, symprec) result(dset)

    real(c_double), intent(in) :: lattice(3, 3)
    real(c_double), intent(in) :: position(3, *)
    integer(c_int), intent(in) :: types(*)
    integer(c_int), intent(in), value :: num_atom
    real(c_double), intent(in), value :: symprec
    type(SpglibDataset) :: dset

    type, bind(c) :: SpglibDataset_c
      integer(c_int) :: spacegroup_number
      integer(c_int) :: hall_number
      character(kind=c_char) :: international_symbol(11)
      character(kind=c_char) :: hall_symbol(17)
      character(kind=c_char) :: choice(6)
      real(c_double) :: transformation_matrix(3, 3)
      real(c_double) :: origin_shift(3)
      integer(c_int) :: n_operations
      type(c_ptr) :: rotations
      type(c_ptr) :: translations
      integer(c_int) :: n_atoms
      type(c_ptr) :: wyckoffs
      type(c_ptr) :: equivalent_atoms
      integer(c_int) :: n_std_atoms
      real(c_double) :: std_lattice(3, 3)
      type(c_ptr) :: std_types
      type(c_ptr) :: std_positions
      character(kind=c_char) :: pointgroup_symbol(6)
    end type SpglibDataset_c

    interface
      function spg_get_dataset_c(lattice_c, position_c, types_c, num_atom_c, symprec_c) &
      & bind(c, name='spg_get_dataset') result(retval)
        import c_int, c_double, c_ptr
        real(c_double), intent(in) :: lattice_c(3, 3)
        real(c_double), intent(in) :: position_c(3, *)
        integer(c_int), intent(in) :: types_c(*)
        integer(c_int), intent(in), value :: num_atom_c
        real(c_double), intent(in), value :: symprec_c
        type(c_ptr) :: retval
      end function spg_get_dataset_c

      subroutine spg_free_dataset_c(dataset) bind(c, name='spg_free_dataset')
        import SpglibDataset_c
        type(SpglibDataset_c), intent(inout) :: dataset
      end subroutine spg_free_dataset_c

    end interface

    type(SpglibDataset_c), pointer :: dset_c
    type(c_ptr) :: dataset_ptr_c
    integer(c_int) :: n_operations, n_atoms, n_std_atoms
    integer :: i
    real(c_double), pointer :: translations(:, :), std_positions(:, :)
    integer(c_int), pointer :: rotations(:, :, :), wyckoffs(:), equivalent_atoms(:)
    integer(c_int), pointer :: std_types(:)

    dataset_ptr_c = spg_get_dataset_c(lattice, position, types, num_atom, symprec)

    if (c_associated(dataset_ptr_c)) then

      dset%spglib_error = SPGLIB_SUCCESS

      call c_f_pointer(dataset_ptr_c, dset_c)

      dset%spacegroup_number = dset_c%spacegroup_number
      dset%hall_number = dset_c%hall_number
      dset%transformation_matrix = dset_c%transformation_matrix
      dset%origin_shift = dset_c%origin_shift
      dset%n_operations = dset_c%n_operations
      dset%n_atoms = dset_c%n_atoms
      dset%n_std_atoms = dset_c%n_std_atoms
      dset%std_lattice = dset_c%std_lattice

      ! Copy C strings to Fortran characters, converting C NULL to Fortran space padded strings
      do i = 1, size(dset_c%international_symbol)
        if (dset_c%international_symbol(i) == C_NULL_CHAR) then
          dset%international_symbol(i:) = ' '
          exit
        end if
        dset%international_symbol(i:i) = dset_c%international_symbol(i)
      end do

      do i = 1, size(dset_c%hall_symbol)
        if (dset_c%hall_symbol(i) == C_NULL_CHAR) then
          dset%hall_symbol(i:) = ' '
          exit
        end if
        dset%hall_symbol(i:i) = dset_c%hall_symbol(i)
      end do

      do i = 1, size(dset_c%choice)
        if (dset_c%choice(i) == C_NULL_CHAR) then
          dset%choice(i:) = ' '
          exit
        end if
        dset%choice(i:i) = dset_c%choice(i)
      end do

      do i = 1, size(dset_c%pointgroup_symbol)
        if (dset_c%pointgroup_symbol(i) == C_NULL_CHAR) then
          dset%pointgroup_symbol(i:) = ' '
          exit
        end if
        dset%pointgroup_symbol(i:i) = dset_c%pointgroup_symbol(i)
      end do

      n_operations = dset_c%n_operations
      n_atoms = dset_c%n_atoms
      n_std_atoms = dset_c%n_std_atoms

      call c_f_pointer(dset_c%rotations, rotations, shape=[3, 3, n_operations])
      call c_f_pointer(dset_c%translations, translations, shape=[3, n_operations])
      call c_f_pointer(dset_c%wyckoffs, wyckoffs, shape=[n_atoms])
      call c_f_pointer(dset_c%equivalent_atoms, equivalent_atoms, shape=[n_atoms])
      call c_f_pointer(dset_c%std_types, std_types, shape=[n_std_atoms])
      call c_f_pointer(dset_c%std_positions, std_positions, shape=[3, n_std_atoms])

      allocate (dset%rotations(3, 3, n_operations))
      allocate (dset%translations(3, n_operations))
      allocate (dset%wyckoffs(n_atoms))
      allocate (dset%equivalent_atoms(n_atoms))
      allocate (dset%std_types(n_std_atoms))
      allocate (dset%std_positions(3, n_std_atoms))

      dset%rotations = rotations
      dset%translations = translations
      dset%wyckoffs = wyckoffs
      dset%equivalent_atoms = equivalent_atoms
      dset%std_types = std_types
      dset%std_positions = std_positions

      call spg_free_dataset_c(dset_c)

    else

      dset%spglib_error = spg_get_error_code()

      dset%spacegroup_number = 0
      dset%hall_number = 0
      dset%international_symbol = ' '
      dset%hall_symbol = ' '
      dset%choice = ' '
      dset%transformation_matrix = 0.0_c_double
      dset%origin_shift = 0.0_c_double
      dset%n_operations = 0
      dset%n_atoms = 0
      dset%n_std_atoms = 0
      dset%std_lattice = 0.0_c_double
      dset%pointgroup_symbol = ' '

    end if

  end function spg_get_dataset


end module spglib_f08

!! Copyright (C) 2021 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module photons_oct_m
  use algorithm_oct_m
  use clock_oct_m
  use debug_oct_m
  use global_oct_m
  use interaction_oct_m
  use iso_c_binding
  use messages_oct_m
  use mpi_oct_m
  use multisystem_debug_oct_m
  use namespace_oct_m
  use parser_oct_m
  use photon_mode_oct_m
  use profiling_oct_m
  use propagator_beeman_oct_m
  use propagator_exp_mid_oct_m
  use propagator_oct_m
  use propagator_verlet_oct_m
  use quantity_oct_m
  use space_oct_m
  use system_oct_m
  use unit_oct_m
  use unit_system_oct_m

  implicit none

  private
  public ::                          &
    photons_t

  type :: photons_t
    private
    type(photon_mode_t), public :: modes
    type(space_t) :: space
    type(namespace_t) :: namespace

  contains
    final :: photons_finalize
  end type photons_t

  interface photons_t
    procedure photons_constructor
  end interface photons_t

contains

  ! ---------------------------------------------------------
  !> The factory routine (or constructor) allocates a pointer of the
  !! corresponding type and then calls the init routine which is a type-bound
  !! procedure of the corresponding type. With this design, also derived
  !! classes can use the init routine of the parent class.
  function photons_constructor(namespace) result(photons)
    class(photons_t),    pointer    :: photons
    type(namespace_t),   intent(in) :: namespace

    PUSH_SUB(photons_constructor)

    SAFE_ALLOCATE(photons)

    photons%namespace = namespace

    call space_init(photons%space, namespace)

    call photon_mode_init(photons%modes, namespace, photons%space%dim)

    POP_SUB(photons_constructor)
  end function photons_constructor

  ! ---------------------------------------------------------
  subroutine photons_finalize(this)
    type(photons_t), intent(inout) :: this

    PUSH_SUB(photons_finalize)

    call photon_mode_end(this%modes)

    POP_SUB(photons_finalize)
  end subroutine photons_finalize

end module photons_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:

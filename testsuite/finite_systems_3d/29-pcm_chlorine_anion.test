# -*- coding: utf-8 mode: shell-script -*-

Test       : Polarizable Continuum Model (PCM)
Program    : octopus
TestGroups : long-run, finite_systems_3d
Enabled    : Yes
Processors : 4

Input      : 29-pcm_chlorine_anion.01-ground_state-n60.inp
match ; SCF convergence               ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 1.72e-13
match ;   eigenvalue [1]                  ; GREPFIELD(static/info, '1   --', 3) ; -17.162746000000002
Precision: 2.79e-05
match ;   eigenvalue [2]                  ; GREPFIELD(static/info, '2   --', 3) ; -5.576105
Precision: 2.79e-05
match ;   eigenvalue [3]                  ; GREPFIELD(static/info, '3   --', 3) ; -5.571308
Precision: 2.79e-05
match ;   eigenvalue [4]                  ; GREPFIELD(static/info, '4   --', 3) ; -5.570442
Precision: 1.35e-07
match ;   electrons-solvent int. energy   ; GREPFIELD(static/info, 'E_e-solvent =', 3) ; -27.074738729999996
Precision: 1.19e-07
match ;   nuclei-solvent int. energy      ; GREPFIELD(static/info, 'E_n-solvent =', 3) ; 23.85930726
Precision: 1.61e-07
match ;   molecule-solvent int. energy    ; GREPFIELD(static/info, 'E_M-solvent =', 3) ; -3.21543148
Precision: 3.92e-07
match ;   electronic pol. charge          ; GREPFIELD(pcm/pcm_info.out, '       9   ', 7) ; -7.84197597
Precision: 3.45e-07
match ;   nuclear pol. charge             ; GREPFIELD(pcm/pcm_info.out, '       9   ', 9) ; 6.90997129

Input      : 29-pcm_chlorine_anion.02-td_prop-n60.inp
Precision: 4.12e-13
match ;    M-solvent int. energy @ t=0       ; GREPFIELD(td.general/energy, '       0', 12) ; -3.215432382877802
Precision: 2.8e-13
match ;    M-solvent int. energy @ t=5*dt    ; GREPFIELD(td.general/energy, '       5', 12) ; -3.215432384511066

Input      : 29-pcm_chlorine_anion.03-ground_state-n60-poisson.inp
match ;  SCF convergence                ; GREPCOUNT(static/info, 'SCF converged') ; 1.0
Precision: 8.60e-05
match ;    eigenvalue [1]                   ; GREPFIELD(static/info, '1   --', 3) ; -17.20144
Precision: 2.81e-05
match ;    eigenvalue [2]                   ; GREPFIELD(static/info, '2   --', 3) ; -5.617953999999999
Precision: 2.81e-05
match ;    eigenvalue [3]                   ; GREPFIELD(static/info, '3   --', 3) ; -5.612844
Precision: 2.81e-05
match ;    eigenvalue [4]                   ; GREPFIELD(static/info, '4   --', 3) ; -5.612465
Precision: 1.35e-07
match ;    electrons-solvent int. energy    ; GREPFIELD(static/info, 'E_e-solvent =', 3) ; -27.07593888
Precision: 1.19e-07
match ;    nuclei-solvent int. energy       ; GREPFIELD(static/info, 'E_n-solvent =', 3) ; 23.86025264
Precision: 1.61e-07
match ;    molecule-solvent int. energy     ; GREPFIELD(static/info, 'E_M-solvent =', 3) ; -3.21568624
Precision: 3.92e-07
match ;    electronic pol. charge           ; GREPFIELD(pcm/pcm_info.out, '       9   ', 7) ; -7.84193583
Precision: 3.45e-07
match ;    nuclear pol. charge              ; GREPFIELD(pcm/pcm_info.out, '       9   ', 9) ; 6.90997129

Input      : 29-pcm_chlorine_anion.04-ground_state-n240.inp
match ;  SCF convergence                ; GREPCOUNT(static/info, 'SCF converged') ; 1.0
Precision: 8.70e-06
match ;    eigenvalue [1]                   ; GREPFIELD(static/info, '1   --', 3) ; -17.393394
Precision: 2.92e-05
match ;    eigenvalue [2]                   ; GREPFIELD(static/info, '2   --', 3) ; -5.832661
Precision: 2.92e-07
match ;    eigenvalue [3]                   ; GREPFIELD(static/info, '3   --', 3) ; -5.830886
Precision: 2.92e-05
match ;    eigenvalue [4]                   ; GREPFIELD(static/info, '4   --', 3) ; -5.830096999999999
Precision: 1.35e-07
match ;    electrons-solvent int. energy    ; GREPFIELD(static/info, 'E_e-solvent =', 3) ; -27.047803190000003
Precision: 1.19e-07
match ;    nuclei-solvent int. energy       ; GREPFIELD(static/info, 'E_n-solvent =', 3) ; 23.83757893
Precision: 1.61e-07
match ;    molecule-solvent int. energy     ; GREPFIELD(static/info, 'E_M-solvent =', 3) ; -3.21022427
Precision: 3.92e-07
match ;    electronic pol. charge           ; GREPFIELD(pcm/pcm_info.out, '       9   ', 7) ; -7.84094993
Precision: 3.45e-06
match ;    nuclear pol. charge              ; GREPFIELD(pcm/pcm_info.out, '       9   ', 9) ; 6.909527199999999
